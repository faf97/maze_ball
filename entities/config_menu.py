import pygame as pg
from .settings import DATA, LENGUAJE, CONFIGS, transition_group, CONFIGS
import sys
from .utils_main import load_dialogs, pos_balls
from .load_music import set_vol_sounds, set_vol_music, play_sound
from .func_json import save_json


dialogs1 = {}


index = -1

def options_menu_configs(key:str,screen,clock):

    if key == "resume":
        DATA["breakconfig"] = True




def draw_balls_vol(screen,pos:tuple,num_vol:float,radius:float,color1=(255,255,255),color2=(200,0,200))->None:
    vol:float = num_vol
    pos = (pos[0]+radius*2,pos[1])
    for i in range(10):

        pos1 = (i*(radius*2)+i*4+pos[0],pos[1])
        rect = pg.Rect(0,0,radius*2,radius*2)
        rect.center = pos1

        pg.draw.circle(screen,color2,pos1,radius)

        if i <= int(num_vol*10) and num_vol > 0.0:
            pg.draw.circle(screen,color1,pos1,radius-3)

        if rect.collidepoint(pg.mouse.get_pos()):
            pg.draw.rect(screen,color1,rect,2)
            if pg.mouse.get_pressed()[0]:
                vol = i/10
    return vol



def comands_touch(i,key,pos,rect,screen,clock,lambda_function):
    global index
    cenrter_pos = rect.center
    rect.width += 10
    rect.center = cenrter_pos
    if rect.collidepoint(pg.mouse.get_pos()):
        pg.draw.rect(screen,(255,255,255),rect,2)
        if i != index:
            play_sound("select3")
            index = i

        if pg.mouse.get_pressed()[0]:
            lambda_function(key,screen,clock)



def draw_configs(pos,dialogs,screen,clock,lambda_function=None):
    first_width = dialogs["volumen"].get_rect().width // 4 * 3
    temp_pos = []
    for i,key in enumerate(dialogs):
        rect:pg.Rect = dialogs[key].get_rect()
        if key in ["video","volumen","resume"]:
            pos1 = (pos[0], rect.height * i + pos[1])
            temp_pos.append(pos1)

        else:
            pos1 = (first_width + pos[0], rect.height * i + pos[1])
            temp_pos.append(pos1)
        screen.blit(dialogs[key],pos1)
        rect.x += 100
        rect.topleft = pos1
        if key == "volmusic":
            volmusic = draw_balls_vol(screen,rect.midright,CONFIGS["vol"],rect.height/2-4)
            if CONFIGS["vol"] != volmusic:
                CONFIGS["vol"] = volmusic
                set_vol_music(volmusic)
        if key == "volsfx":
            volsound = draw_balls_vol(screen,rect.midright,CONFIGS["volsound"],rect.height/2-4)
            if CONFIGS["volsound"] != volsound:
                CONFIGS["volsound"] = volsound
                set_vol_sounds(volsound)

        comands_touch(i,key,pos1,rect,screen,clock,lambda_function)

    return temp_pos





def config_menu(screen,clock,screenshot):
    screenshot2 = screenshot.copy()
    global DATA
    global dialogs1
    DATA["breakconfig"] = False

    if len(dialogs1) == 0:
        dialogs1 = load_dialogs(LENGUAJE[CONFIGS["lenguaje"]],"configmenu1")

    shadow = pg.surface.Surface((screen.get_width(),screen.get_height()))
    shadow.set_colorkey(DATA["colorkey"])
    shadow.set_alpha(100)
    screenshot2.blit(shadow,(0,0))
    while True:
        keys = pg.key.get_pressed()
        screen.fill(DATA["bg"])
        clock.tick(DATA["fps"])
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    DATA["breakconfig"] = True




        screen.blit(screenshot2,(0,0))

        draw_configs((100,100),dialogs1,screen,clock,options_menu_configs)
        transition_group.update(1/DATA["fps"],0,transition_group)
        transition_group.draw(screen)



        if DATA["breakconfig"] and not pg.mouse.get_pressed()[0]:
            break

        pg.display.update()
    save_json(CONFIGS,"configs.json")




