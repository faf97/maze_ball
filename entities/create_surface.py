from pygame.surface import Surface
from pygame.draw import circle
from .settings import DATA

def create_surface(size,color,format):
    '''format: 1= circle, 2 = rect'''
    image = Surface(size).convert()
    image.fill(DATA["colorkey"])
    image.set_colorkey(DATA["colorkey"])
    image.set_alpha(255)
    if format == 1:
        rect = image.get_rect()
        circle(image,color,rect.center,(rect.width+rect.height)/4)

    if format == 2:
        image.fill(color)
    return image