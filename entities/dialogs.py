from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QFileDialog
from PyQt5 import QtCore
import sys




def saveFileDirectory(typeFiles = "All Files (*);;Python Files (*.py);;Text Files (*.txt)"):
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(400,400,300,300)
    win.setWindowTitle("CodersLegacy")
    file , check = QFileDialog.getSaveFileName(None, "QFileDialog getSaveFileName() Demo",
                                               "", typeFiles)
    if check:
        return file


def selectFile(typeFiles = "All Files (*);;Python Files (*.py);;Text Files (*.txt)"):
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(400,400,300,300)
    win.setWindowTitle("CodersLegacy")
    file , check= QFileDialog.getOpenFileName(None, "QFileDialog.getOpenFileName()",
                                               "", typeFiles)
    if check:
        return file


def selectDirectory():
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(400,400,300,300)
    win.setWindowTitle("CodersLegacy")
    file = QFileDialog.getExistingDirectory()
    return file


