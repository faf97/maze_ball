from pygame.image import load
from pygame.surface import Surface
from pygame import Rect
from pygame import mixer
from .sprite import Sprite
from .settings import CONFIGS, DATA
from .timer import Timer





class End(Sprite):
    def __init__(self,path_next_map="",*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.image = load("imgs/all/end.png").convert()
        self.path_next_map = path_next_map
        self.type = "end"


    def get_path_map(self):
        return self.path_next_map


    def load_sprite(self,path,size=(16,16)):
        lst = []
        img = load(path)
        for i in range(img.get_height()//size[0]):
            surf = Surface(size)
            surf.set_colorkey(DATA["colorkey"])
            surf.blit(img,(0,0),Rect(i*size[0],i*size[1],size[0],size[1]))
            lst.append(surf)
        return lst





