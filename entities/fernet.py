from cryptography.fernet import Fernet
from .func_json import read_json, save_json


def encrypt(text:str):
    key = b'y86EviC6LOIiPc-cOopsgHbpMXLsKB1RqjcDx7RsIys='
    cifrado = Fernet(key)
    texto_encriptado = cifrado.encrypt(str.encode(text))
    return str(texto_encriptado)[2:-1]


def decrypt(text:str):
    key = b'y86EviC6LOIiPc-cOopsgHbpMXLsKB1RqjcDx7RsIys='
    cifrado = Fernet(key)
    t = str.encode(text)
    decrypt_text = cifrado.decrypt(t)
    return decrypt_text.decode()



def decrypt_save(d:dict)->dict:
    new_d = {}
    for key in d:

        if key in ["deaths","totaltime","points"]:
            new_d[key] = float(decrypt(d[key]))
        else:
            new_d[key] = decrypt(d[key])
    return new_d



def encrypt_save(d:dict)->dict:
    new_d = {}
    for key in d:
        if key in ["startmap","deaths","totaltime","points"]:
            new_d[key] = encrypt(str(d[key]))
    return new_d




