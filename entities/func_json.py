import json
import pygame as pg


def read_json(path):
    with open(path, 'r') as f:
      data = json.load(f)
    return data

def save_json(data,path):
    json_object = json.dumps(data, indent=4)
    with open(path, "w") as outfile:
        outfile.write(json_object)





