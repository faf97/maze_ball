from .shoot import Anim



class Gold( Anim ):
    def __init__( self, *args, **kwargs ):
        super( ).__init__( *args, **kwargs )
        self.type = "gold"



class FadeGold( Anim ):
    def __init__( self, pos, path, group, layer=0, speed=0.05, subtract=-1 ):
        super( ).__init__( pos, path, group, layer, speed )
        self.type = "fadegold"
        self.image.set_alpha( 255 )
        self.subtract = subtract

    def fade( self ):
        a = self.image.get_alpha( )
        if a <= 0:
            self.kill( )
        a += self.subtract
        self.image.set_alpha( a )


    def update( self, dt, degree, group ):
        super( ).update( dt, degree, group )
        self.fade( )




