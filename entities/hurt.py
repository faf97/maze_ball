from .sprite import Sprite
from .timer import Timer
from math import cos, pi
from .settings import DATA



class Hurt(Sprite):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.type = "hurt"



class Lava(Hurt):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.timer = Timer(55,0.5)
        self.image.set_alpha(0)
        self.degree = 0



    def update_alpha(self):
        self.degree += 1
        self.image.set_alpha(200+self.timer.get_int_timer())
        if self.timer.get_int_timer() == 55:
            self.timer.timer = self.timer.speed*-1


    def update(self,dt,degree,group):
        self.timer.update()
        self.update_alpha()




