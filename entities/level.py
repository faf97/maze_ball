from pygame.sprite import Sprite, LayeredUpdates
from pygame.surface import Surface
from pygame.math import Vector2
from pygame.transform import scale, rotate
from pygame.mouse import get_pos, get_pressed
from math import cos, sin, atan2, pi
from .settings import DATA, group




class Level:
    def __init__(self,pos,size,layer,color):
        self.image_aux = Surface(size)
        self.image = self.image_aux.copy()
        self.color_fill = color
        self.image_aux.fill(color)
        self.image_aux.set_colorkey(DATA["colorkey"])
        self.pos = Vector2(pos)
        self.rect = self.image.get_rect(center=pos)
        self._layer = layer
        self.angle = 0
        self.aux_pos = Vector2()
        self._shake = False


    def create_shake(self, num):
        if not self._shake:
            self._shake = True
            if num < 0:
                self.angle = num*-1
            else:
                self.angle = num


    def update_shake(self):
        if self.angle > 0:
            self.angle -= 0.2
            self.aux_pos.x = cos(self.angle)*self.angle
            self.aux_pos.y = sin(self.angle)*self.angle
        else:
            self._shake = False


    def rot_img(self,angle)->None:
        self.image = rotate(self.image_aux,angle)
        self.rect = self.image.get_rect(center=self.rect.center)


    def rotating(self)->int:
        x, y = get_pos()
        x2, y2 = self.pos
        angle = atan2(x2-x,y2-y)*180/pi+180
        return angle


    def draw(self,screen,group):
        self.image_aux.fill(self.color_fill)
        group.draw(self.image_aux)
        x,y = self.rect.topleft
        screen.blit(self.image,(x+self.aux_pos.x,y+self.aux_pos.y))


    def update(self,dt,group)->None:
        degrees = self.rotating()
        group.update(dt,degrees,group)
        self.rot_img(degrees)
        self.update_shake()








