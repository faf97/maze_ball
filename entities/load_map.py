from pygame.sprite import LayeredUpdates
from pygame.image import load
from pygame.surface import Surface
from .sprite import Sprite
from .player import Player
from .end import End
from .func_json import read_json
from .settings import DATA
from .load_materials import load_all_materials
from .hurt import Hurt, Lava
from .gold import Gold
from .shoot import Shooter



def draw_material_surface(surface:Surface,type,name_texture="gpoint"):
    materials = load_all_materials("imgs/materials.json")
    m = materials
    w, h = surface.get_width(), surface.get_height()
    stw, sth = DATA["sizetile"]
    rw, rh = w // DATA["sizetile"][0] + 1, h // DATA["sizetile"][1] + 1
    for x in range(rw):
        for y in range(rh):
            surface.blit(m[name_texture],(x*stw,y*sth))


def div_data(string:str)->list:
    lst_dot_dot = string.split(":")[:-1]
    lst_eat = []
    for data in lst_dot_dot:
        lst_eat.append(data.split(","))
    return lst_eat


def clear_sprites_in_group(group:LayeredUpdates):
    for sprite in group.sprites():
        sprite.kill()


def load_map(pos,path:str,group:LayeredUpdates)->None:
    clear_sprites_in_group(group)
    lst_data = div_data(read_json(path))
    for data in lst_data:
        x, y, w, h, l, t = data

        if t == "player":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            Player(pos_,size_,1,group,DATA["colorkey"],image=load("imgs/all/player.png").convert())
            continue


        if t == "hurt":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            img = Surface(size_)
            draw_material_surface(img,t)
            Hurt(pos_,size_,int(l),group,DATA["colorkey"],image=img)
            continue

        if t == "lava":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            img = Surface(size_)
            draw_material_surface(img,t,name_texture=t)
            Lava(pos_,size_,4,group,DATA["colorkey"],image=img)
            continue

        if t == "black":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            img = Surface(size_)
            draw_material_surface(img,t,name_texture=t)
            lava = Lava(pos_,size_,4,group,DATA["colorkey"],image=img)
            lava.type = "black"
            continue

        if t == "gold":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            img = Surface(size_)
            draw_material_surface(img,t,name_texture=t)
            g = Gold(pos_,"imgs/all/gold.png",group,layer=-4)
            g.timer.randon()
            continue

        if t == "end":
            pos_ = (int(x),int(y))
            img = Surface(size_)
            draw_material_surface(img,t,name_texture=t)
            e = End(h,pos_,(16,16),0,group,DATA["colorkey"],image=img)
            e.rect.width = 16
            e.rect.height = 16
            continue

        if t == "shooter":
            pos_ = (int(x),int(y))
            size_ = (int(w),int(h))
            img = Surface(size_)
            g = Shooter(pos_,"imgs/all/shooter.png",group,layer=16)
            g.timer.randon()
            g.random_degree()
            continue


        pos_ = (int(x),int(y))
        size_ = (int(w),int(h))
        img = Surface(size_)
        draw_material_surface(img,t,name_texture=t)
        Sprite(pos_,size_,-1,group,DATA["colorkey"],image=img)











