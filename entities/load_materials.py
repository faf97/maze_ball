from pygame.image import load
from .func_json import read_json
from .settings import DATA


def load_img(path:str):
    img = load(path)
    img.set_colorkey(DATA["colorkey"])
    return img


def load_all_materials(path:str)->dict:
    materials = read_json(path)
    loaded_imgs = {}
    for key in materials:
        loaded_imgs[key] = load_img(materials[key])
    return loaded_imgs




