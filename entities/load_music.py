from pygame import mixer
from .func_json import read_json
from pygame import mixer
from random import randint
from os import listdir
from .settings import *





def get_files(name,path="./"):
    contenido = listdir(path)
    return [file for file in contenido if file[len(file)-len(name):] == name]


def list_to_dict_sounds(path,lst:list)->dict:
    d = {}
    for file in lst:
        key = file.split(".")[0]
        d[key] = mixer.Sound(f"{path}{file}")
    return d


def load_sounds(path):
    sounds_dict = list_to_dict_sounds(path,get_files(".wav",path))
    return sounds_dict


def play_sound(name_sound):
    SOUNDS[name_sound].stop()
    SOUNDS[name_sound].play()


def load_music()->dict:
    musics = read_json("music/music.json")
    return musics



def set_vol_sounds(num:float)->None:
    for key in SOUNDS:
        SOUNDS[key].set_volume(num)


def set_vol_music(vol:float):
    mixer.music.set_volume(vol)




def get_random_dict(dictionary:dict):
    return list(dictionary.items())[randint(0,len(dictionary)-1)][0]



def play_random_music():
    mixer.music.set_volume(CONFIGS["vol"])
    play_music(get_random_dict(MUSIC))


def play_music(name_music):
    mixer.music.load(MUSIC[name_music])
    mixer.music.play()


def update_state_music():
    if not mixer.music.get_busy():
        play_random_music()
