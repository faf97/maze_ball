import pygame as pg
import sys
##from rot import Rot
from particles2 import Particle2
from random import randint




class Env:
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode((0,0))
        self.clock = pg.time.Clock()
        self.sprites = pg.sprite.LayeredUpdates()
        self.sprites2 = pg.sprite.LayeredUpdates()
##    self.rot = Rot(self.screen.get_rect().center,(256,256),self.sprites)
##    self.rot2 = Rot(self.screen.get_rect().center,(32,32),self.sprites2,color=(0,255,0))


    def add_particle(self):
        if pg.mouse.get_pressed()[0]:
            for _ in range(40):
                degree = randint(0,360)
                layer = randint(0,2)
                speed = randint(128,256)
#            r = randint(0,255)
#            g = randint(100,255)
#            b = randint(100,255)
                Particle2(pg.mouse.get_pos(),(48,48),degree,speed,layer,self.sprites)#,color=(r,g,b),format=1)


    def dt(self):
        fps = self.clock.get_fps()
        dt = 0
        if fps > 0:
            dt = 1 / fps
        return dt


    def main(self):
        while True:
            self.screen.fill((39,39,39))
            self.clock.tick(60)

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()

            dt = self.dt()
            self.add_particle()
            self.sprites.update(dt)
            self.sprites.draw(self.screen)

            self.sprites2.update(dt)
            self.sprites2.draw(self.screen)


            pg.display.update()




if __name__ == "__main__":
    Env().main()




