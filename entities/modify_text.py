'''Editor files text by legi'''
import os



def change_chars(string,chr1,to_chr2):
    string = string.split(chr1)
    for i,c in enumerate(string):
        if c == "":
            string[i] = to_chr2
    return "".join(string)



def save_file(array,path="App.py"):
    with open(path,"w") as file:
        for line in array:
            file.write(line)
        file.close()


def read_file(chr1,chr2,path=""):
    lst = []
    with open (path,"r") as file:
        for line in file:
            lst.append(change_chars(line,chr1,chr2))
    return lst

def refactor_files():
    list_name_file = os.listdir()
    for i,file in enumerate(list_name_file):
##print(list_name_file[i][len(list_name_file[i])-3:])
        if list_name_file[i][len(list_name_file[i])-3:] == ".py":
##print(file)
            save_file(read_file(chr(9),chr(32)*4,file),file)



if __name__ == "__main__":
    refactor_files()








