##from .sprite import Sprite
from pygame.mouse import get_pos
from pygame.image import load
from .settings import DATA
from pygame.sprite import Sprite
##from pygame.surface import Surface





class Mouse(Sprite):
    def __init__(self,layer,path,group):
        super().__init__(group)
        self.aux_image = load("imgs/all/mouse.png").convert()
        self.aux_image.set_colorkey(DATA["colorkey"])
        self.image = self.aux_image.copy()
        self.rect = self.image.get_rect(center=get_pos())
        self._layer = layer



    def set_visible(self,boolean:bool):
        pass


    def update(self,dt,degree,group):
        self.rect.center = get_pos()

