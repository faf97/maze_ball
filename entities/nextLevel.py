from pygame.image import load
from pygame.surface import Surface
from pygame import Rect
from .sprite import *
from .settings import *
from .timer import Timer


class NextLevel(Sprite):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.lst = self.load_sprite("imgs/#.png",(self.image.get_width(),self.image.get_height()))
        self.image = self.lst[0].copy()
        self.timer = Timer(len(self.lst)-1,0.1)


    def load_sprite(self,path,size=(16,16)):
        lst = []
        img = load(path)
        for i in range(img.get_height()//size[0]):
            surf = Surface(size)
            surf.set_colorkey(DATA["colorkey"])
            surf.blit(img,(0,0),Rect(i*size[0],i*size[1],size[0],size[1]))
            lst.append(surf)
        return lst


    def update(self,dt,degrees,group):
##        print(self.timer.get_int_timer())
        self.image.blit(self.lst[self.timer.get_int_timer()],(0,0))
        self.timer.update()
##        if self.timer.get_limit() == self.timer.get_int_timer():
##            self.timer.reset()


    def __str__(self):
        return f"Class: {End}"



