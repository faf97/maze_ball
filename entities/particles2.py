import pygame as pg
from math import atan2, cos, sin, pi
from .create_surface import create_surface
##from .gold import Gold

DATA = {"colorkey":(255,0,255)}


class Particle2(pg.sprite.Sprite):
    def __init__(self,pos,size,degree,speed,layer,group,color=(255,255,255),format=1,subtract=-1):
        self.image = create_surface(size,color,format)
        self.subtract_bool = False
        self.set_alpha(self.image,subtract)
        self.mask = pg.mask.from_surface(self.image)
        self.pos = pg.math.Vector2(pos)
        self.rect = self.image.get_rect(center=pos)
        self.old_rect = self.rect.copy()
        self.degree = degree
        self.speed = speed
        self._layer = layer
        self.subtract = subtract
        self.direction = pg.math.Vector2((1,1))
        self.type = "particle2"
        super().__init__(group)


    def set_alpha(self,image:pg.surface.Surface,subtract):
        if subtract > 0:
            image.set_alpha(0)
            self.subtract_bool = False
        if subtract < 0:
            image.set_alpha(255)
            self.subtract_bool = True


    def update_size(self):
        self.timer.get_integer_timer()


    def update_direction(self,dt):
        rad = self.degree*pi/180
        speed_dt = self.speed * dt
        x, y = cos(rad) * speed_dt, sin(rad) * speed_dt
        self.pos.x += x * self.direction.x
        self.pos.y += y * self.direction.y
        self.rect.center = self.pos


    def fade(self):
        alpha = self.image.get_alpha()

        if self.subtract_bool:
            if alpha <= 0:
                self.kill()

        if not self.subtract_bool:
            if alpha >= 255:
                self.kill()

        a = alpha + self.subtract
        self.image.set_alpha(a)


    def collision(self,degree,groups):
        #all collisions
        for sprite in groups.sprites():
            rect2 = sprite.rect

            if sprite.__hash__() == self.__hash__():
                continue

            if rect2.colliderect(self.rect) and not isinstance(sprite,(Particle2)):
                self.logic_collide(rect2)


    def logic_collide(self,rect2):
            #up collision
            if self.rect.bottom >= rect2.top and self.old_rect.bottom <= rect2.top:
                self.direction.y *= -1

            #down collision
            if self.rect.top <= rect2.bottom and self.old_rect.top >= rect2.bottom:
                self.direction.y *= -1

            if self.rect.right >= rect2.left and self.old_rect.right <= rect2.left:
                self.direction.x *= -1

            if self.rect.left <= rect2.right and self.old_rect.left >= rect2.right:
                self.direction.x *= -1


    def update(self,dt,degree,group):
        self.old_rect = self.rect.copy()
        self.update_direction(dt)
        self.collision(degree,group)
        self.fade()

