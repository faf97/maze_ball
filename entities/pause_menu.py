import pygame as pg
import sys
from .settings import DATA, SAVE, CONFIGS, LENGUAJE, SELECTED_LENGUAJE, transition_group, screenshot
from .config_menu import config_menu
from .utils_main import load_dialogs, draw_options, pos_balls
from .func_json import save_json
from .fernet import encrypt_save
from .stats_menu import stats_menu


dialogs = {}




def options_menu_pause(key:str,screen,clock):
    if key == "exit":
        pg.quit()
        sys.exit()

    if key == "mainmenu":
        DATA["breakpause"] = True
        DATA["breakgame"] = True

    if key == "resume":
        DATA["breakpause"] = True

    if key == "config":
        config_menu(screen,clock,screenshot)

    if key == "save":
        SAVE["startmap"] = CONFIGS["auxmap"]
        save_json(encrypt_save(SAVE),"save/save.json")
        DATA["breakpause"] = True

    if key == "stats":
        screenshot.fill(DATA["bg"])
        stats_menu(screen,clock,screenshot)






def pause_menu(screen,clock,screenshot,pos):
    global DATA
    global dialogs
    DATA["breakpause"] = False
    break_loop2 = False
    if len(dialogs) == 0:
        dialogs = load_dialogs(LENGUAJE[CONFIGS["lenguaje"]],"pause")

    shadow = pg.surface.Surface((screen.get_width(),screen.get_height()))
    shadow.set_colorkey(DATA["colorkey"])
    shadow.set_alpha(100)
    screenshot.blit(shadow,(0,0))

    while True:


        screen.fill(DATA["bg"])
        clock.tick(DATA["fps"])
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()


        screen.blit(screenshot,(0,0))
        pos_balls(draw_options(screen,clock,dialogs,options_menu_pause),transition_group)
        transition_group.update(1/DATA["fps"],0,transition_group)
        transition_group.draw(screen)

        draw_options(screen,clock,dialogs)


        if DATA["breakpause"] and not pg.mouse.get_pressed()[0]:
            break


        pg.display.update()
    pg.mouse.set_pos(pos)