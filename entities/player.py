from pygame.key import get_pressed
from pygame import K_a, K_s, K_d, K_w, Rect
from pygame.transform import scale
from pygame.mouse import get_pressed
from pygame import mixer
from pygame import mask
from pygame.sprite import collide_mask
from .sprite import *
from .hurt import Hurt
from .particles2 import Particle2
from .gold import Gold, FadeGold
from .shoot import Shoot
from .settings import CONFIGS, clock, SAVE
from .load_music import play_sound
from math import cos, sin, pi
from random import randint
from sys import exit
from pygame import quit




class Player(Sprite):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.old_rect = self.rect.copy()
        self.momentum = Vector2()
        self.direction = Vector2()
        self.gravity = 9.8
        self.speed = 24
        self.healt = 100
        self.collide = Vector2()
        self.type = "player"
        self.change_map = False
        self.points = 0


    def move(self):
        keys = get_pressed()
        if keys[K_w]:
            self.up()
        if keys[K_s]:
            self.dw()
        if keys[K_a]:
            self.lt()
        if keys[K_d]:
            self.rt()


    def up(self):
        self.rect.y -= 1

    def dw(self):
        self.rect.y += 1

    def lt(self):
        self.rect.x -= 1

    def rt(self):
        self.rect.x += 1


    def update_pos(self):
        """momentum ===> pos ===> rect"""
        self.pos.x += self.momentum.x
        self.pos.y += self.momentum.y
        if self.pos.magnitude() == 0.0:
            self.pos.normalize()
        self.rect.x = self.pos.x
        self.rect.y = self.pos.y


    def get_end(self,rect2:Rect):
        if self.rect.colliderect(rect2):
            x1, y1 = self.rect.center
            x2, y2 = rect2.center
            if x1 == x2 and y1 == y2:
                return True
        return False


    def get_points(self):
        copy = self.points
        self.points = 0
        return copy



    def collision(self,degree,groups):
        #all collisions
        for sprite in groups.sprites():
            rect2 = sprite.rect

            if self.rect.colliderect(rect2) and isinstance(sprite,(Gold)):
                play_sound("bell1")
                self.points += 1
                FadeGold(rect2.topleft,"imgs/all/gold_touch.png",groups,layer=-8,subtract=-5)
                sprite.kill()

            if sprite.__hash__() == self.__hash__():
                continue

            if rect2.colliderect(self.rect) and not isinstance(sprite,(Hurt,Particle2,Gold,FadeGold)) and sprite.type != "end":
                self.logic_collide(rect2)

            if rect2.colliderect(self.rect) :
                if isinstance(sprite,(Hurt)):
                    self.create_kill_particles(degree,groups)
                    play_sound("explo2")
                    continue

                if isinstance(sprite,(Shoot)) and collide_mask(self,sprite) != None:
                    self.create_kill_particles(degree,groups)
                    play_sound("explo2")
                    sprite.kill()
                    continue

                if sprite.type == "end":
                    self.change_map = sprite.rect.colliderect(self.rect)
                    continue
                else:#if sprite.rect.colliderect(self.rect) and sprite.type != "end":
                    self.change_map = False
                    continue



    def create_move_particles(self,group):
        if CONFIGS["particles"]:
##            degree = randint(0,360)
##            speed = randint(128,256)
##            r = randint(0,255)
##            g = randint(100,255)
##            b = randint(100,255)
            Particle2(self.rect.center,(8,8),0,0,100,group,subtract=-5)
##            Particle2(self.rect.center,(8,8),degree,16,-1,group,color=(r,g,b),subtract=-5)


    def kill_particle(self,degree,group):
        angle = int(degree * pi/180)
        a = 180+angle
        b = 360+angle
        degree = randint(a,b)
        speed = randint(32,64)
        Particle2(self.rect.center,(16,16),degree,speed,-1,group)
        self.kill()


    def create_kill_particles(self,degree,group):
        if CONFIGS["particles"]:
            for _ in range(40):
                angle = int(degree * pi/180)
                a = 180+angle
                b = 360+angle
                degree = randint(a,b)
                speed = randint(32,64)
                Particle2(self.rect.center,(16,16),degree,speed,-1,group)
        else:
            self.kill_particle(degree,group)
        self.kill()


    def logic_collide(self,rect2):
            #up collision
            vec2 = Vector2((self.momentum.x, self.momentum.y))
            if self.rect.bottom >= rect2.top and self.old_rect.bottom <= rect2.top:
                if self.rect.bottom != rect2.top and self.old_rect.bottom != rect2.top:
                    play_sound("goal1")
                self.rect.y = rect2.top-self.rect.height
                self.pos.y = self.rect.y
                self.momentum.y *= -1


            #down collision
            if self.rect.top <= rect2.bottom and self.old_rect.top >= rect2.bottom:
                if self.rect.top != rect2.bottom and self.old_rect.top != rect2.bottom:
                    play_sound("goal1")
                self.rect.y = rect2.bottom
                self.pos.y = self.rect.y
                self.momentum.y *= -1


            if self.rect.right >= rect2.left and self.old_rect.right <= rect2.left:
                if self.rect.right != rect2.left and self.old_rect.right != rect2.left:
                    play_sound("goal1")
                self.rect.x = rect2.left-self.rect.width
                self.pos.x = self.rect.x
                self.momentum.x *= -1


            if self.rect.left <= rect2.right and self.old_rect.left >= rect2.right:
                if self.rect.left != rect2.right and self.old_rect.left != rect2.right:
                    play_sound("goal1")
                self.rect.x = rect2.right
                self.pos.x = self.rect.x
                self.momentum.x *= -1



    def update_momentum(self,dt,degrees):
        rad = ((degrees + 90) * pi / 180)#
        self.momentum.x += cos(rad) * self.gravity / pi * dt * self.speed
        self.momentum.y += sin(rad) * self.gravity / pi * dt * self.speed


    def decrease_speed_horizontal(self):
        if self.momentum.x >0:
            if self.momentum.x - 0.3 < 0:
                self.momentum.x = 0.0
            else:
                self.momentum.x -= 0.3
        if self.momentum.x < 0.0:
            if self.momentum.x + 0.3 > 0.0:
                self.momentum.x = 0.0
            else:
                self.momentum.x += 0.3


    def decrease_speed_vertical(self):
        if self.momentum.y > 0.0:
            if self.momentum.y - 0.3 < 0.0:
                self.momentum.y = 0
            else:
                self.momentum.y -= 0.3
        if self.momentum.y < 0.0:
            if self.momentum.y + 0.3 > 0.0:
                self.momentum.y = 0.0
            else:
                self.momentum.y += 0.3

    def update_goal_sound(self):
        if self.rect.x == 0 and int(self.collide.x) == 0:
            play_sound("goal1")
            self.collide.x = 1
        else:
            self.collide.x = 0

        if self.rect.y == 0 and int(self.collide.y) == 0:
            play_sound("goal1")
            self.collide.x = 1
        else:
            self.collide.y = 0


    def update(self,dt,degree,group):
        self.old_rect = self.rect.copy()
        self.update_momentum(dt,degree)
        self.update_pos()
        self.collision(degree,group)
        self.decrease_speed_horizontal()
        self.decrease_speed_vertical()
##        self.update_goal_sound()
        self.create_move_particles(group)



