from pygame.font import SysFont

def render_text(text,type_font='Leelawadee UI',color=(255,255,255),size_font=20):
    my_font = SysFont(type_font, size_font)
    return my_font.render(text,False,color).convert()