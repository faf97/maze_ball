import pygame as pg
import sys
from .settings import DATA, CONFIGS, transition_group, clock
from .load_music import play_sound
from .func_json import read_json, save_json
from .load_music import get_files






index = -1

def load_flags(path:str)->dict:
    d = read_json(path)
    new_d = {}
    for k in d:
        new_d[k] = pg.image.load(d[k]).convert_alpha()
    return new_d



def selected_actions(key:str):
    global CONFIGS
    global DATA
    CONFIGS["lenguaje"] = key
    DATA["breaklenguaje"] = True
    save_json(CONFIGS,"configs.json")



def draw_flags(screen,pos,dict_flags:dict):
    global index
    for i,k in enumerate(dict_flags):

        rect = dict_flags[k].get_rect()
        rect.topleft = pos_temp = (pos[0],pos[1]+i*rect.height+i*10)
        screen.blit(dict_flags[k],pos_temp)
        if rect.collidepoint(pg.mouse.get_pos()):
            pg.draw.rect(screen,(255,255,255),rect,2)
            if index != i:
                play_sound("select3")
                index = i
            if pg.mouse.get_pressed()[0]:
                selected_actions(k)
                play_sound("select2")


def select_lenguaje(screen,clock:tuple,screenshot):
    global DATA
    flags_dict = load_flags("imgs/flags/select_lenguaje.json")
    DATA["breaklenguaje"] = False
    break_loop2 = False
    shadow = pg.surface.Surface((screen.get_width(),screen.get_height()))
    shadow.set_colorkey(DATA["colorkey"])
    shadow.set_alpha(100)
    screenshot.blit(shadow,(0,0))

    while True:

        screen.fill(DATA["bg"])
        clock.tick(DATA["fps"])
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    pg.quit()
                    sys.exit()

        pos = screen.get_rect().center
        draw_flags(screen,(pos[0]-50,200),flags_dict)
        if DATA["breaklenguaje"] and not pg.mouse.get_pressed()[0]:
            break


        pg.display.update()




