import pygame as pg
import os

#[54,74,97]
#"colorwater":[251,94,117]
DATA = {"screensize":[1024,768],"colorkey":[255,0,255],"colorwater":[255,94,150],"bg":[43,0,85],"fps":144,"sizetile":[16,16],\
"finish_game":False,"points":0,"nextmap":False,"breakgame":False,"breakpause":False,"breakstats":False,"breaklenguaje":False,"breakconfig":False}
CONFIGS = {}
MUSIC = {}
SOUNDS = {}
LENGUAJE = {}
SAVE = {}
SELECTED_LENGUAJE = ""



os.environ["SDL_VIDEODRIVER"] = "windows"
os.environ["SDL_VIDEO_CENTERED"] = "1"
pg.mixer.pre_init(frequency=44100, size=-16, channels=1, buffer=64)
pg.init()


screen = pg.display.set_mode(DATA["screensize"])#,pg.NOFRAME|pg.FULLSCREEN)
pg.display.set_caption("Maze Ball by @sombritaok")
clock = pg.time.Clock()



#la capa 0 es para la entidad end
#la capa 16 es para la entidad shooter
#la capa 20 es para los disparos (Shoot)
#las monedas(Gold) deben estar en la capa -4 para luego poder comprobar si en esa capa hay monedas
#el Player se encontrará en la capa 1 para luego verificar si esa capa está vacía
#la Lava se encuentra en la capa 4


group = pg.sprite.LayeredUpdates()
transition_group = pg.sprite.LayeredUpdates()
screenshot = pg.surface.Surface((screen.get_width(),screen.get_height()))

