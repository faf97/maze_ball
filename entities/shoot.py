import pygame as pg


from .settings import *
from .timer import Timer
from .particles2 import Particle2
##from .settings import SOUNDS
from .load_music import play_sound
from math import cos, sin, atan2, pi
from random import randint



def load_anim(path):
        lst = []
        img = pg.image.load(path)
        w, h = img.get_width(), img.get_height()
        for i in range(w//h):
            surface = pg.surface.Surface((h,h))
            surface.set_colorkey(DATA["colorkey"])
            surface.fill(DATA["colorkey"])
            surface.blit(img,(0,0),pg.Rect(i*h,0,h,h))
            lst.append(surface)
        return lst


class Shoot(Particle2):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        pg.draw.circle(self.image,(0,190,0),self.image.get_rect().center,self.rect.width//2,2)


    def collision(self,degree,groups):
        #all collisions
        for sprite in groups.sprites():
            rect2 = sprite.rect

            if sprite.__hash__() == self.__hash__():
                continue

            if rect2.colliderect(self.rect):
                if isinstance(sprite,(Particle2)) and sprite.type != "gold" and sprite.type != "shooter" and sprite.type != "particle2":
                    self.logic_collide(rect2,degree,groups)



    def logic_collide(self,rect2,degree,group):
            #up collision
            if self.rect.bottom >= rect2.top and self.old_rect.bottom <= rect2.top:
                self.create_kill_particles(degree,group)
                self.direction.y *= -1


            #down collision
            if self.rect.top <= rect2.bottom and self.old_rect.top >= rect2.bottom:
                self.create_kill_particles(degree,group)
                self.direction.y *= -1



            if self.rect.right >= rect2.left and self.old_rect.right <= rect2.left:
                self.create_kill_particles(degree,group)
                self.direction.x *= -1



            if self.rect.left <= rect2.right and self.old_rect.left >= rect2.right:
                self.create_kill_particles(degree,group)
                self.direction.x *= -1


    def limit_kill(self,degree,group):
        if self.rect.left <= 16:
            self.create_kill_particles(degree,group)
        if self.rect.right >= 384:
            self.create_kill_particles(degree,group)
        if self.rect.top <= 16:
            self.create_kill_particles(degree,group)
        if self.rect.bottom >= 384:
            self.create_kill_particles(degree,group)


    def kill_particle(self,degree,group):
        angle = int(degree * pi/180)
        a = 180+angle
        b = 360+angle
        degree = randint(a,b)
        speed = randint(32,64)
        Particle2(self.rect.center,(16,16),degree,speed,8,group,color=(80,0,80))
        self.kill()


    def create_kill_particles(self,degree,group):
        if CONFIGS["particles"]:
            for _ in range(40):
                angle = int(degree * pi/180)
                a = 225
                b = 315
                degree = randint(a,b)
                speed = randint(32,64)
                Particle2(self.rect.center,(16,16),degree+self.degree,speed,0,group,color=(80,0,80),subtract=-7)
        else:
            self.kill_particle(degree,group)
        self.kill()
        play_sound("shoot3")

    def update(self,dt,degree,group):
        self.old_rect = self.rect.copy()
        self.update_direction(dt)
        self.collision(degree,group)
        self.limit_kill(degree,group)




class Anim(pg.sprite.Sprite):
        def __init__(self,pos,path,group,layer=0,speed=0.05):
            self.anim = load_anim(path)
            self.image = pg.surface.Surface((self.anim[0].get_width(),self.anim[0].get_height()))
            self.image.set_colorkey(DATA["colorkey"])
            self.image.convert()
            self.image.blit(self.anim[2],(0,0))
            self.rect = self.image.get_rect(topleft=pos)
            self.timer = Timer(len(self.anim),speed)
            self._layer = layer
            self.type = "anim"
            self.mask = pg.mask.from_surface(self.image)
            super().__init__(group)


        def update_time(self):
            self.timer.update()
            if self.timer.get_limit() == self.timer.get_int_timer()+1:
                self.timer.reset()


        def update_anim(self):
            self.image.fill(DATA["colorkey"])
            self.image.blit(self.anim[int(self.timer.get_int_timer())],(0,0))



        def update(self,dt,degree,group):
            self.update_time()
            self.update_anim()



class AnimKill(Anim):
    def __init__(self,dt,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.timer = Timer(len(self.anim)-1,dt)
        self.type = "anim"

    def update_kill(self):
        self.timer.update()


    def update(self,dt,degree,group):
        super().update(dt,degree,group)



class Shooter(Anim):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.degree = 0
        self.timer2 = Timer(3,1/DATA["fps"])
        self.type = "shooter"


    def create_move_particles(self,group):
        speed = 64
        layer = 20
        sbt = 0
        degree = 45
        for i in range(8):
            Shoot(self.rect.center,(16,16),self.degree+i*degree,speed,layer,group,color=(0,0,0),subtract=sbt)#(255,145,21)




    def update_timer2(self,group):
        self.timer2.update()
        if self.timer2.get_int_timer() == self.timer2.get_limit():
            self.create_move_particles(group)
            self.degree += 20
            self.timer2.reset()
            SOUNDS["shoot1"].play()

    def random_degree(self):
        self.degree = randint(0,360)


    def update(self,dt,degree,group):
        super().update(dt,degree,group)
        self.update_timer2(group)




