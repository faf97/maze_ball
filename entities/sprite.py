from pygame import sprite
from pygame.surface import Surface
from pygame.math import Vector2
from .settings import DATA
from pygame import mask


class Sprite(sprite.Sprite):
    def __init__(self,pos,size,layer,group,color,image=None):
        if not image is None:
            self.image = image
            self.image.set_colorkey(DATA["colorkey"])
        else:
            self.image = Surface(size)
            self.image.fill(color)
        self.mask = mask.from_surface(self.image)
        self.type = type
        self.pos = Vector2(pos)
        self.rect = self.image.get_rect(topleft=pos)
        self._layer = layer
        self.type = "sprite"
        super().__init__(group)
