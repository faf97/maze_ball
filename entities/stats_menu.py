import pygame as pg
import sys
from .settings import DATA, SAVE, CONFIGS, LENGUAJE, SELECTED_LENGUAJE, transition_group, screenshot
from .config_menu import config_menu
from .utils_main import load_dialogs, draw_options, pos_balls, render_text
from .func_json import save_json
from .fernet import encrypt_save


dialogs = {}

stats = {
    "points": None,
    "deaths": None,
    "totaltime": None
    }



def draw_surfs(screen,rect,surface):
    pass



def render_stats(stats):
    for key in stats:
        if key == "totaltime":
            stats[key] = render_text(f"{str(int(SAVE[key]/60))}'")
        else:
            stats[key] = render_text(str(int(SAVE[key])))


def options_menu_stats(key:str,screen,clock):
    if key == "back":
        DATA["breakstats"] = True



def draw_stats(screen,clock,dialogs:dict,stats:dict,options_menu_stats):
    for i, key in enumerate(stats):
        rect = dialogs[key].get_rect()
        pos = (100, rect.height * 2 * i + 100)
        rect.topleft = pos
        rect_stats = dialogs[key].get_rect(midleft=rect.midright)
        rect_stats.x += 10
        stats[key].blit(screen,pos)
        rect2 = stats[key].get_rect()
        screen.blit(dialogs[key],pos)
        screen.blit(stats[key],rect_stats.topleft)
##        options_menu_stats(key,screen,clock)



def stats_menu(screen,clock,screenshot):
    global DATA
    global dialogs
    global stats
    render_stats(stats)
    if len(dialogs) == 0:
        dialogs = load_dialogs(LENGUAJE[CONFIGS["lenguaje"]],"stats")
    DATA["breakstats"] = False
    break_loop2 = False
    shadow = pg.surface.Surface((screen.get_width(),screen.get_height()))
    shadow.set_colorkey(DATA["colorkey"])
    shadow.set_alpha(100)
    screenshot.blit(shadow,(0,0))



    while True:
        screen.fill(DATA["bg"])
        clock.tick(DATA["fps"])
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    DATA["breakstats"] = True

        screen.blit(screenshot,(0,0))
        draw_stats(screen,clock,dialogs,stats,options_menu_stats)


        if DATA["breakstats"] and not pg.mouse.get_pressed()[0]:
            break


        pg.display.update()
