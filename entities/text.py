from pygame.sprite import Sprite
from .render_text import render_text




class Text(Sprite):
    def __init__(self,pos,layer,group,sizefont=20,color=(255,255,255),fade=False,subtract=-1):
        self.image = render_text(text,color=color,size_font=sizefont)
        self.image.set_alpha(255)
        self.rect = self.image.get_rect(topleft=pos)
        self.fade = fade
        self.subtract = subtract


    def fade(self):
        a = self.image.get_alpha()
        if a <= 0:
            self.kill()
        a -= self.subtract
        self.image.set_alpha(a)

    def update(self,dt,degree,group):


