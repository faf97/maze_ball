from random import randint

class Timer:
    def __init__(self,time:int,speed:float):
        self.time = time
        self.timer = 0
        self.speed = speed

    def reset(self):
        self.timer = 0

    def get_limit(self):
        return self.time

    def get_int_timer(self):
        return int(self.timer)

    def randon(self):
        self.timer = randint(0,self.time-1)

    def update(self):
        if int(self.timer+self.speed) <= self.time:
            self.timer += self.speed
##        else:
##            self.timer = 0