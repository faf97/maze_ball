import tkinter as tk

LENGUAJE = {"acept":"Aceptar","path":"Directorio del siguiente mapa:","config":"Configuración"}



class Entry:
    def __init__(self,text=""):
        self.root = tk.Tk()
        self.root.title(LENGUAJE["config"])
        self.root.resizable(False,False)
        self.root.protocol("WM_DELETE_WINDOW", self.exit)
        self.label = tk.Label(self.root,text=LENGUAJE["path"],padx=20,pady=10)
        self.label.grid(column=5,row=0)

        self.entry1 = tk.Entry(self.root)
        self.entry1.grid(column=5,row=1)
        self.set_text(text)

        self.label2 = tk.Label(self.root,text=LENGUAJE["path"],padx=20,pady=10)
        self.label2.grid(column=5,row=0)


        self.but = tk.Button(self.root,text=LENGUAJE["acept"],command=self.exit)
        self.but.grid(column=5,row=10)


        self.text = text



    def exit(self):
        self.text = self.entry1.get()
        self.root.destroy()

    def set_text(self,text):
        self.entry1.insert(0,text)


    def mainloop(self):
        self.root.mainloop()
        return self.text


if __name__ == "__main__":
    print(Entry().mainloop())