from pygame.surface import Surface
from pygame.sprite import Sprite, LayeredUpdates
from pygame.mask import from_surface
from pygame.draw import rect
from pygame import Rect
from .create_surface import create_surface
from .settings import DATA



class Transition(Sprite):
    def __init__(self,pos,size,degree,layer,group,color=None,colorborders=None,subtract=-1):
        if color is None:
            self.image = create_surface(size,DATA["bg"],2)
        else:
            self.image = create_surface(size,color,2)
        self.rect = self.image.get_rect(center=pos)
        self._layer = layer
        self.degree = degree
        self.mask = from_surface(self.image)
        self.rect_var = Rect(0,0,1,1)
        self.rect_var.center = pos
        self.subtract = subtract
        self.subtract_bool = False
        self.color = color
        self.colorborders = colorborders
        self.config_subtract()
        super().__init__(group)


    def config_subtract(self):
        if self.subtract > 0:
            self.rect_var = Rect(self.rect.x,self.rect.y,1,1)
            self.subtract_bool = False
        if self.subtract < 0:
            self.rect_var = self.rect.copy()
            self.subtract_bool = True


    def fade(self,group):
        sizewh = self.rect_var.width

        if self.subtract_bool:
            if sizewh <= 0:
                self.kill()
##                self.add_invert_transition(group)

        if not self.subtract_bool:
            if sizewh >= self.rect.width:
                self.kill()
##                self.add_invert_transition(group)

        a = sizewh + self.subtract
        self.rect_var.width = a
        self.rect_var.height = a
        self.rect_var.center = self.rect.center


##    def add_invert_transition(self,group):
##        Transition(self.rect.center,(self.rect.width,self.rect.height),self.degree,self._layer,group,self.color,self.colorborders,self.subtract*-1)


    def update_image(self):
        self.image.fill(DATA["colorkey"])
        if self.color is None:
            rect(self.image,DATA["bg"],self.rect_var)
        else:
            rect(self.image,self.color,self.rect_var)
        if not self.colorborders is None:
            rect(self.image,self.colorborders,self.rect_var,2)


    def update(self,dt,degree,group):
        self.fade(group)
        self.update_image()






