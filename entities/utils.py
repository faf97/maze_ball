from pygame.surface import Surface
from pygame.mouse import get_pos

def create_img(size:tuple or list,fill:bool=True)->Surface:
    img = Surface(size)
    img.set_colorkey(DATA["colorkey"])
    if fill:
        img.fill(DATA["colorkey"])
    return img


def mouse_tile_pos(size_tile:tuple or list)->tuple:
    x,y = get_pos()
    tx, ty = size_tile
    px = x // tx * tx
    py = y // ty * ty
    return (px,py)