import pygame as pg
import sys
from random import randint
from .render_text import render_text
from .transition import Transition
from.settings import DATA, CONFIGS, SAVE, SOUNDS
from .load_map import load_map
from .func_json import save_json
from .particles2 import Particle2
from .load_music import play_sound
from .fernet import encrypt_save #decrypt_save,
import time


index = -1

pressed = False


def reset_saved():
    for key in SAVE:
        SAVE[key] = 0


def draw_options(screen,clock,dialogs,lambda_function=None)->list:
    list_rects = []
    size = 20
    global pressed
    global index
    for i,key in enumerate(dialogs):
        img = dialogs[key]
        pos = (100,100+i*img.get_height())

        rect:pg.Rect = img.get_rect(topleft=pos)
        rect.width += size
        rect.center = (rect.center[0] - size // 2, rect.center[1])

        screen.blit(dialogs[key],pos)

        if rect.collidepoint(pg.mouse.get_pos()):
            list_rects.append(rect)
            if index != i:
                play_sound("select3")
                index = i
            if key == "continue" and CONFIGS["startmap"] == "":
                pg.draw.rect(screen,(100,100,100),rect,2)
            else:
                pg.draw.rect(screen,(255,255,255),rect,2)
            if not lambda_function is None and pg.mouse.get_pressed()[0]:
                play_sound("select2")
                lambda_function(key,screen,clock)

##        if not rect.collidepoint(pg.mouse.get_pos()):
##            index = -1
    return list_rects
    pressed = False




def set_global_data(files:dict,global_dict:dict):
    for key in files:
        global_dict[key] = files[key]



def load_dialogs(dialogs:dict,key_dict)->dict:
    new_dict = {}
    for key in dialogs[key_dict]:
        path_img = f"imgs/lenguaje/{CONFIGS['lenguaje']}/{key}.png"
        new_dict[key] = pg.image.load(path_img)
    return new_dict



def render_dialogs(dialogs:dict,key_dict="menu")->dict:
    new_dict = {}
    for key in dialogs[key_dict]:
        new_dict[key] = render_text(dialogs[key_dict][key])
    return new_dict


def dt(clock):
    dt = 0
    fps = clock.get_fps()
    if fps > 0:
        dt = 1/fps
    return dt


def kill_sprites(group:pg.sprite.LayeredUpdates):
    for sprite in group.sprites():
        sprite.kill()



def load_entities(group:pg.sprite.LayeredUpdates,map_path:str):
    if CONFIGS["startmap"] == "end":
        kill_sprites(group)
        DATA["finish_game"] = True
    else:
        try:
            load_map((400,400),map_path,group)
        except:
            DATA["finish_game"] = True



def add_transition(screen,transition_group,color,subtract=10):
    rect = screen.get_rect()
    size = (rect.width,rect.height)
    Transition(rect.center,size,10,10,transition_group,color,subtract=subtract)




def pause_load(screen,clock,screenshot,color,group,transition_group,map_path):
    CONFIGS["auxmap"] = map_path
    add_transition(screen,transition_group,color,subtract=10)
    play_sound("transition1")
    while True:
        screen.fill(DATA["bg"])
        clock.tick(DATA["fps"])
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()


        pg.mouse.set_pos(screen.get_rect().center)
        transition_group.remove_sprites_of_layer(6)

        if len(transition_group.get_sprites_from_layer(10)) == 0:
            load_entities(group,map_path)
            break



        transition_group.update(1/DATA["fps"],0,transition_group)
        transition_group.draw(screenshot)
        screen.blit(screenshot,(0,0))


        pg.display.update()

    save_json(encrypt_save(SAVE),"save/save.json")
    pg.mouse.set_pos((screen.get_width()//2,screen.get_height()-64))
    screenshot.blit(screen,(0,0))

    add_transition(screen,transition_group,color,subtract=-10)
    play_sound("transition1reverse")





def create_move_particles(group,pos,transition_group):
    degree = randint(0,360)
    speed = randint(128,256)
    r = randint(0,255)
    g = randint(100,255)
    b = randint(100,255)
    Particle2(pos,(16,16),degree,16,0,transition_group,color=(r,g,b),subtract=-10)




def pos_balls(list_rects,transition_group):
    if len(list_rects) > 0:
        rect = list_rects[0]
        x, y = rect.midleft
        create_move_particles(transition_group,(x-16,y),transition_group)


def create_circle_img(self,size:int,color:tuple=(255,255,255),colorborders:tuple=None):
    surface = pg.surface.Surface((size,size))
    surface.fill(DATA["colorkey"])
    surface.set_colorkey(DATA["colorkey"])
    pg.draw.circle(surface,color,surface.get_rect().center,size/2)
    if not colorborders is None:
        pg.draw.circle(surface,colorborders,surface.get_rect().center,size/2)


def time_control(func):
    init = time.time()
    def run(*args,**kwargs):

        func(*args,**kwargs)
        global SAVE
        SAVE["totaltime"] += time.time()-init
        return None
    return run






