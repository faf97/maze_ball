# -*- coding: utf-8 -*-
import pygame as pg
import sys
import os
from entities import *
from random import randint
from entities.end import End
from entities import timer, transition
from entities.particles2 import Particle2
from entities.mouse import Mouse
from entities.load_map import load_map
from entities.timer import Timer
from entities.utils_main import add_transition, draw_options, dt,kill_sprites,\
load_entities, pause_load, load_dialogs, set_global_data, pos_balls,\
create_move_particles,time_control, reset_saved
from entities.config_menu import config_menu
from entities.stats_menu import stats_menu
from entities.select_lenguaje import select_lenguaje
import time
import asyncio








class Env:
    def __init__(self,screen,clock,group,transition_group):
        self.screen = screen
        self.clock = clock
        self.group = group
        self.transition_group = transition_group
        self.bg = Level(self.screen.get_rect().center ,(400,400),0,DATA["colorwater"])
        self.timer_reset = timer.Timer(2,1/DATA["fps"])


    def contains_gold(self)->bool:
        return len(self.group.get_sprites_from_layer(-4)) != 0


    def contains_player(self):
        return len(self.group.get_sprites_from_layer(1)) != 0


    def verify_level(self,screenshot):
        if not self.contains_player():
            self.bg.create_shake(60)
            self.timer_reset.update()
            if self.timer_reset.get_limit() == self.timer_reset.get_int_timer():
                screenshot.blit(self.screen,(0,0))
                pause_load(screen,clock,screenshot,DATA["bg"],self.group,self.transition_group,CONFIGS["startmap"])
                self.timer_reset.reset()
                DATA["points"] = 0
                SAVE["deaths"] += 1

                save_json(encrypt_save(SAVE),"save/save.json")

        player_lst = self.group.get_sprites_from_layer(1)

        if len(player_lst) == 1 and player_lst[0].change_map and not self.contains_gold():
            end = self.group.get_sprites_from_layer(0)[0]
            if len(end.path_next_map.split(".map")) == 2:
                play_sound("magic3")
                SAVE["points"] += DATA["points"]
                save_json(encrypt_save(SAVE),"save/save.json")
                screenshot.blit(self.screen,(0,0))
                DATA["points"] = 0
                pause_load(screen,clock,screenshot,DATA["bg"],self.group,self.transition_group,end.path_next_map)
            else:
                if end.path_next_map == "end":
                    DATA["finish_game"] = True


    def add_points(self):
        player_lst = self.group.get_sprites_from_layer(1)
        if len(player_lst) > 0:
            DATA["points"] += player_lst[0].get_points()


    @time_control
    def main(self)->None:
        DATA["finish_game"] = False
        DATA["breakgame"] = False
        while not DATA["finish_game"]:
            keys = pg.key.get_pressed()
            self.screen.fill(DATA["bg"])
            self.clock.tick(DATA["fps"])

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()

            update_state_music()
            self.bg.update(dt(self.clock),self.group)
            self.bg.draw(self.screen,self.group)
            self.transition_group.update(1/DATA["fps"],0,self.transition_group)

            if keys[pg.K_ESCAPE]:
                screenshot.blit(self.screen,(0,0))
                pause_menu(self.screen,self.clock,screenshot,pg.mouse.get_pos())



            self.verify_level(screenshot)
            self.transition_group.draw(screen)

            self.add_points()

            if DATA["breakgame"]:
                DATA["points"] = 0
                break

            pg.display.update()
##            await asyncio.sleep(0)





class Menu:
    def __init__(self,screen,clock,group,transition_group):
        self.screen = screen
        self.clock = clock
        self.group = group
        self.transition_group = transition_group
        self.timer = timer.Timer(3,1/DATA["fps"])
        self.dialogs = load_dialogs(LENGUAJE[CONFIGS["lenguaje"]],"menu")
        img = pg.surface.Surface((16,16)).convert()
        img.set_colorkey((0,0,0))
        play_random_music()
        pg.mouse.set_visible(False)
        Mouse(6,"imgs/all/mouse.png",transition_group)



    def options_menu_principal(self,key:str,screen,clock):
        if key == "exit":
            save_json(encrypt_save(SAVE),"save/save.json")
            pg.quit()
            sys.exit()


        if key == "play":
            reset_saved()
            screenshot.blit(screen,(0,0))
            pause_load(self.screen,self.clock,screenshot,DATA["colorwater"],self.group,self.transition_group,CONFIGS["startmap"])
            Env(screen,clock,self.group,self.transition_group).main()


        if key == "continue" and SAVE["startmap"] != "" and SAVE["startmap"] != "end":
            screenshot.blit(screen,(0,0))
            pause_load(self.screen,self.clock,screenshot,DATA["colorwater"],self.group,self.transition_group,SAVE["startmap"])
            Env(screen,clock,self.group,self.transition_group).main()


        if key == "training":
            screenshot.blit(screen,(0,0))
            pause_load(self.screen,self.clock,screenshot,DATA["colorwater"],self.group,self.transition_group,CONFIGS["training"])
            Env(screen,clock,self.group,self.transition_group).main()


        if key == "config":
            screenshot.fill(DATA["bg"])
            config_menu(screen,clock,screenshot)


        if key == "stats":
            screenshot.fill(DATA["bg"])
            stats_menu(self.screen,self.clock,screenshot)



    async def main(self):
        pressed = True
        while True:
            if DATA["finish_game"]:
                set_global_data(read_json("configs.json"),CONFIGS)
            DATA["finish_game"] = False
            keys = pg.key.get_pressed()
            self.screen.fill(DATA["bg"])
            self.clock.tick(DATA["fps"])
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()


            self.transition_group.update(1/DATA["fps"],1,self.transition_group)
            pos_balls(draw_options(screen,clock,self.dialogs,self.options_menu_principal),self.transition_group)
            self.transition_group.draw(self.screen)
            pg.display.update()
            await asyncio.sleep(0)




##def main()


if __name__ == '__main__':
    set_global_data(decrypt_save(read_json("save/save.json")),SAVE)
    set_global_data(load_sounds("sounds/"),SOUNDS)
    set_global_data(read_json("configs.json"),CONFIGS)
    set_global_data(load_music(),MUSIC)
    set_global_data(read_json("lenguaje.json"),LENGUAJE)
    set_vol_sounds(CONFIGS["volsound"])
##    if CONFIGS["lenguaje"] == "":
##        screenshot.fill(DATA["bg"])
##        select_lenguaje(screen,clock,screenshot)
    app = Menu(screen,clock,group,transition_group)
    asyncio.run(app.main())
    SAVE["totaltime"] = 0




