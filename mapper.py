import pygame as pg
from sys import exit
from entities import read_json, save_json
from entities import load_all_materials
from entities.utils import *
from entities.dialogs import saveFileDirectory
from entities.tk_entry_mapper import Entry

'''
para crear los rects dinamicos voy a apretar click izquierdo y el codigo verificará
si colisiona y si los lados lt rt top bottom son iguales al del mouse para modificar el tamaño mdel rect

para que funcione el rectangulo se creará con otro rectangulo de fondo mas grande y se seteara el centro
cada vez que cambie de tamaño

'''




DATA = {"screensize":[1024,768],"bg":[39,39,39],"colorkey":[255,0,255],"fps":60,"sizetile":[16,16]}



materials = load_all_materials("imgs/materials.json")




def create_img(size:tuple or list,fill:bool=True)->pg.surface.Surface:
    img = pg.surface.Surface(size)
    img.set_colorkey(DATA["colorkey"])
    if fill:
        img.fill(DATA["colorkey"])
    return img


def mouse_tile_pos(size_tile):
    x,y = pg.mouse.get_pos()
    tx, ty = size_tile
    px = x // tx * tx
    py = y // ty * ty
    return (px,py)


class LayerUpdate(pg.sprite.LayeredUpdates):
    def __init__(self):
        super().__init__()


    def draw_rects(self,screen):
        for sprite in self.sprites():
            sprite.draw_rects(screen)


class RectTile(pg.sprite.Sprite):
    ''' types
    g = grass
    d = dirt
    s = sand
    m = metal
    f = fluorecent
    n = neon
    '''
    def __init__(self,pos,group,layer=0,name=None,size=(16,16),name_texture="",image=None,id=0):
        '''agregar group_particles, para dibujar los efectos de particulas'''

        self.id = id
        self.name = name
        if not image is None:
            self.image = image
        else:
            self.image = self.create_image(size,type)
        self.rect = self.image.get_rect(topleft=pos)
        self.rect_touch = self.create_rect_touch(self.rect)
        self._layer = layer
        self.name_texture = name_texture
        self.path_next_map = "end"
        self.entry = None#Entry(self.name_texture)
        super().__init__(group)


    def create_image(self,size,type):
        img = self.create_img(size)
        img.fill((255,0,0))
        return img


    def create_rect_touch(self,rect_tile:pg.Rect)->pg.Rect:
        w, h = rect_tile.width, rect_tile.height
        stw, sth = DATA["sizetile"]
        rect = pg.Rect(0,0,w+int(stw*2),int(h+sth*2))
        rect.center = rect_tile.center
        return rect


    def create_img(self,size:tuple or list,fill:bool=True)->pg.surface.Surface:
        img = pg.surface.Surface(size)
        img.set_colorkey(DATA["colorkey"])
        if fill:
            img.fill(DATA["colorkey"])
        return img


    def rezize_img(self):
        '''no se está reasignando la imagen del reescalado'''
        if self.rect.width != self.image.get_width() or self.rect.height != self.image.get_height():
            self.image = self.create_img((self.rect.width,self.rect.height))



    def draw_rects(self,screen:pg.surface.Surface)->None:
        pg.draw.rect(screen,(255,0,0),self.rect,2)
        pg.draw.rect(screen,(255,0,254),self.rect_touch,2)


    def get_data(self,pos:tuple)->str:
        if self.name_texture == "end":
            data = f"{self.rect.x-pos[0]},{self.rect.y-pos[1]},{self.rect.width},{self.path_next_map},{self._layer},{self.name_texture}:"
        else:
            data = f"{self.rect.x-pos[0]},{self.rect.y-pos[1]},{self.rect.width},{self.rect.height},{self._layer},{self.name_texture}:"

        return data

    def config_end(self):
##        keys = pg.key.get_pressed()
        if self.name_texture == "end" and self.rect.collidepoint(pg.mouse.get_pos()) and pg.mouse.get_pressed()[1]:
            self.entry = Entry(self.path_next_map)
            self.path_next_map = self.entry.mainloop()


    def update(self)->None:
        self.rezize_img()
        self.config_end()



class Entities:
    def __init__(self):
        self.entities = LayerUpdate()
        self.rect_mouse = pg.Rect(0,0,DATA["sizetile"][0],DATA["sizetile"][1])
        self.layer = 0
        self.textures = read_json("imgs/materials.json")

        self.material_selected = "gpoint"
        self.name_texture_selected = "gpoint"
        w, h = DATA["sizetile"]
        self.canvas = pg.Rect(10*w,5*h,25*w,25*h)
        self.id_aded = 0
        self.id_selected = 0


    def get_data_text(self):
        string = ""
        for sprite in self.entities.sprites():
            string = f"{string}{sprite.get_data(self.canvas.topleft)}"
        return string


    def draw_all_textures(self,screen):
        type_material = materials
        for i,key in enumerate(type_material):
            pos = (688,i*DATA["sizetile"][1] + DATA["sizetile"][1])
            screen.blit(type_material[key],pos)
            rect: pg.Rect = type_material[key].get_rect(topleft=pos)
            if pg.mouse.get_pressed()[0] and rect.collidepoint(pg.mouse.get_pos()):
                self.name_texture_selected = key


    def modify_rect(self):
        keyboards = pg.key.get_pressed()
        for sprite in self.entities.sprites():
            rt : pg.Rect = sprite.rect_touch
            rtile : pg.Rect = sprite.rect
            if rtile.collidepoint(pg.mouse.get_pos()):
                self.id_selected = sprite.id

            if pg.mouse.get_pressed()[0] and self.name_texture_selected == sprite.name_texture and self.canvas.colliderect(self.rect_mouse) and sprite.id == self.id_selected:
                self.add_size_rect(rt,rtile)
            if pg.mouse.get_pressed()[2] and self.name_texture_selected == sprite.name_texture:
                self.del_size_rect(rt,rtile)
            self.rezize_img(sprite)
            if self.del_rect_bool(rtile):
                sprite.kill()
            if keyboards[pg.K_DELETE] and self.rect_mouse.colliderect(rtile):
                sprite.kill()


    def draw_material_surface(self,surface:pg.surface.Surface,type):
        m = materials
        w, h = surface.get_width(), surface.get_height()
        stw, sth = DATA["sizetile"]
        rw, rh = w // DATA["sizetile"][0] + 1, h // DATA["sizetile"][1] + 1
        for x in range(rw):
            for y in range(rh):
                surface.blit(m[self.name_texture_selected],(x*stw,y*sth))



    def verify_vertex(self,rect:pg.Rect,group:pg.sprite.LayeredUpdates):
        ''' terminar función, debe verificar que las esquinas no atraviesen los rectangulos '''
        dict_vertex = {"tl":rect.topleft,"tr":rect.topright,"bl":rect.bottomleft,"br":rect.bottomright}
        for sprite in group.sprites():
            rect2 = sprite.rect
            dict_vertex2 = {"tl":rect2.topleft,"tr":rect2.topright,"bl":rect2.bottomleft,"br":rect2.bottomright}



    def verify_collide(self,rtile2:pg.Rect)->None:
        for sprite in self.entities.sprites():
            rtile : pg.Rect = sprite.rect
            if rtile2.colliderect(rtile) and rtile.x != rtile2.x and rtile.y != rtile2.y:
                return True
        return False


    def rezize_img(self,sprite):
        if sprite.rect.width != sprite.image.get_width() or sprite.rect.height != sprite.image.get_height():
            pos = sprite.rect.topleft
            size = (sprite.rect.width,sprite.rect.height)
            sprite.kill()
            r = RectTile(pos,self.entities,size=size,name_texture=self.name_texture_selected,id=sprite.id)
            self.draw_material_surface(r.image,r.name_texture)





    def add_size_rect(self,rt,rtile):
            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.top == rt.top:
                rtile.height += DATA["sizetile"][1]
                rt.height += DATA["sizetile"][1]
                rtile.top -= DATA["sizetile"][1]
                rt.center = rtile.center
                if self.verify_collide(rtile):
                    rtile.height -= DATA["sizetile"][1]
                    rt.height -= DATA["sizetile"][1]
                    rtile.top += DATA["sizetile"][1]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.left == rt.left:
                rtile.width += DATA["sizetile"][0]
                rt.width += DATA["sizetile"][0]
                rtile.left -= DATA["sizetile"][0]
                rt.center = rtile.center
                if self.verify_collide(rtile):
                    rtile.width -= DATA["sizetile"][0]
                    rt.width -= DATA["sizetile"][0]
                    rtile.left += DATA["sizetile"][0]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.right == rt.right:
                rtile.width += DATA["sizetile"][0]
                rt.width += DATA["sizetile"][0]
                rt.center = rtile.center
                if self.verify_collide(rtile):
                    rtile.width -= DATA["sizetile"][0]
                    rt.width -= DATA["sizetile"][0]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.bottom == rt.bottom:
                rtile.height += DATA["sizetile"][1]
                rt.height += DATA["sizetile"][1]
                rt.center = rtile.center
                if self.verify_collide(rtile):
                    rtile.height -= DATA["sizetile"][1]
                    rt.height -= DATA["sizetile"][1]
                    rt.center = rtile.center


    def del_size_rect(self,rt,rtile):
            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.top == rt.top:
                if rtile.height - DATA["sizetile"][1] > 0:
                    rtile.height -= DATA["sizetile"][1]
                    rt.height -= DATA["sizetile"][1]
                    rtile.top += DATA["sizetile"][1]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.left == rt.left:
                if rtile.width - DATA["sizetile"][0] > 0:
                    rtile.width -= DATA["sizetile"][0]
                    rt.width -= DATA["sizetile"][0]
                    rtile.left += DATA["sizetile"][0]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.right == rt.right:
                if rtile.width - DATA["sizetile"][0] > 0:
                    rtile.width -= DATA["sizetile"][0]
                    rt.width -= DATA["sizetile"][0]
                    rt.center = rtile.center

            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])) and self.rect_mouse.bottom == rt.bottom:
                if rtile.height - DATA["sizetile"][1] > 0:
                    rtile.height -= DATA["sizetile"][1]
                    rt.height -= DATA["sizetile"][1]
                    rt.center = rtile.center


    def del_rect_bool(self,rtile:pg.Rect)->bool:
        if pg.mouse.get_pressed()[2]:
            b = rtile.top == self.rect_mouse.top
            b = b and rtile.left == self.rect_mouse.left
            b = b and rtile.right == self.rect_mouse.right
            return b and rtile.bottom == self.rect_mouse.bottom
        return False


    def collide_entities(self)->bool:
        for sprite in self.entities.sprites():
            rt = sprite.rect_touch
            if rt.collidepoint(mouse_tile_pos(DATA["sizetile"])):
                return True
        return False


    def update_rect_mouse(self):
        self.rect_mouse.topleft = mouse_tile_pos(DATA["sizetile"])


    def contain(self,layeredupdates:pg.sprite.LayeredUpdates,name:str)->bool:
        for sprite in layeredupdates:
            if sprite.name == name:
                return True
        return False


    def insert(self):
        pos = mouse_tile_pos(DATA["sizetile"])
        string_pos = f"{pos[0]}{pos[1]}"
        if pg.mouse.get_pressed()[0] and not self.contain(self.entities,string_pos)\
                 and not self.collide_entities() and self.canvas.collidepoint(pg.mouse.get_pos()):
            img = materials[self.name_texture_selected]
            RectTile(pos,self.entities,name=string_pos,name_texture=self.name_texture_selected,id=self.id_aded,image=img)
            self.id_aded += 1


    def draw_all(self,screen):
        pg.draw.rect(screen,(0,255,255),self.rect_mouse,2)
        self.entities.draw(screen)
        self.entities.draw_rects(screen)
        self.draw_all_textures(screen)
        pg.draw.rect(screen,(255,255,255),self.canvas,2)


    def update(self):
        self.update_rect_mouse()
        self.insert()
        self.modify_rect()
        self.entities.update()



class MapperBallGame:
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode(DATA["screensize"])
        pg.display.set_caption("Mapper beta 1 by @SoyLegi")
        self.clock = pg.time.Clock()
        self.entities = Entities()


    def main(self):


        while True:

            self.screen.fill(DATA["bg"])
            self.clock.tick(DATA["fps"])
            keys = pg.key.get_pressed()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    exit()
                if event.type == pg.KEYDOWN:
                    if keys[pg.K_LCTRL] and event.key == pg.K_s:
                        path = saveFileDirectory("Map File (*.map)")
                        if not path is None:
                            data = self.entities.get_data_text()
                            save_json(data,path)



            self.entities.update()
            self.entities.draw_all(self.screen)

            pg.display.update()




if __name__ == '__main__':
    MapperBallGame().main()
